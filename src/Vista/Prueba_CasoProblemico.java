/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;

/**
 *
 * @author madarme
 */
public class Prueba_CasoProblemico {
    
    
    public static void main(String[] args) {
        String nombres[]={"Brayan", "Carlos","David","Gederson"};
        Integer codigos[]={1152030,1152031,1152032};
        Float notas[]={3.4F,4F,5F,3.2F};
        Persona personas[]={new Persona(1,"Brayan",3,4,2000),new Persona(2,"Gederson",4,14,1999)};
        
        //Caso problémico :(
        //Imprimir datos:
        
        String msg="";
        for(String datos:nombres)
        {
            msg+=datos+"\t";
        }
        System.out.println("Vector de string:"+msg);
        
        msg="";
        for(Integer datos:codigos)
        {
            msg+=datos+"\t";
        }
        System.out.println("Vector de Integer:"+msg);
        
        
        
         msg="";
        for(Float datos:notas)
        {
            msg+=datos+"\t";
        }
        System.out.println("Vector de Float:"+msg);
        
        
         msg="";
        for(Persona datos:personas)
        {
            msg+=datos.toString()+"\t";
        }
        System.out.println("Vector de Integer:"+msg);
        
    }
    
}
